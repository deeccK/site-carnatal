$(document).ready(function() {

    $(".owl-carousel").owlCarousel({
        loop: true,
        video: true,
        dots: true,
        nav: true,
        pagination: true,
        navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
        items: 1
    });

    $('.owl-carousel').owlCarousel({
        callbacks: true,
        onInitialized: moveNav
    });

    function moveNav() {
        $('.owl-nav').appendTo('.owl-stage-outer');
    }

    // Passar slides com teclado/passador
    document.addEventListener('keyup', function(event) {
        var owl = jQuery(".owl-carousel");

        // handle cursor keys
        if (event.keyCode == 37) {
            // go left

            owl.trigger('prev.owl.carousel');


        } else if (event.keyCode == 39) {
            // go right
            owl.trigger('next.owl.carousel');

        }
    });



});